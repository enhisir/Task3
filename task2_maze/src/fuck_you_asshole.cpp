#include "ros/ros.h"
#include "actionlib_msgs/GoalStatusArray.h"
#include "actionlib_msgs/GoalStatus.h"
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/Twist.h"

ros::Publisher pose_pub;
ros::Publisher cmd_vel_pub;
ros::Subscriber status_sub;

void MoveBehind();
void PublishStandardPose();
void NeedPoseNavGoal(const actionlib_msgs::GoalStatusArrayConstPtr& msg);

int main(int argc, char **argv)
{
  /**
   * The ros::init() function needs to see argc and argv so that it can perform
   * any ROS arguments and name remapping that were provided at the command line.
   * For programmatic remappings you can use a different version of init() which takes
   * remappings directly, but for most command-line programs, passing argc and argv is
   * the easiest way to do it.  The third argument to init() is the name of the node.
   *
   * You must call one of the versions of ros::init() before using any other
   * part of the ROS system.
   */
  ros::init(argc, argv, "fuck_you_asshole");

  /**
   * NodeHandle is the main access point to communications with the ROS system.
   * The first NodeHandle constructed will fully initialize this node, and the last
   * NodeHandle destructed will close down the node.
   */
  ros::NodeHandle n;
  ROS_INFO("NODE STARTED");

  pose_pub = n.advertise<geometry_msgs::PoseStamped>("move_base_simple/goal", 1000);
  cmd_vel_pub = n.advertise<geometry_msgs::Twist>("cmd_vel", 1000);
  status_sub = n.subscribe<actionlib_msgs::GoalStatusArray>("move_base/status", 1000, NeedPoseNavGoal);
  ROS_INFO("INITIALIZATON SUCCESSFUL");
  
  ros::spin();

  return 0;
}

void MoveBehind()
{
    const double standard_velocity = 0.5d;
    geometry_msgs::Twist tw;
    tw.linear.x = standard_velocity;
    pose_pub.publish(tw);
    ROS_INFO("MOVING BEHIND");
    ros::Duration(0.5).sleep();
    tw.linear.x = 0;
    pose_pub.publish(tw);
    ROS_INFO("MOVING STOPPED");
}

void PublishStandardPose()
{
    double x = -26.0d;
    double y = -25.0d;
    double z = 0.0d;
    geometry_msgs::PoseStamped pose_msg;
    pose_msg.header.frame_id = "odom";
    // pose_msg.header.stamp = ros::Time::now();
    pose_msg.pose.position.x = x;
    pose_msg.pose.position.y = y;
    pose_msg.pose.position.z = z;
    pose_msg.pose.orientation.x = 0;
    pose_msg.pose.orientation.y = 0;
    pose_msg.pose.orientation.z = 0;
    pose_msg.pose.orientation.w = 1;
    pose_pub.publish(pose_msg);
    ROS_INFO("PUBLISHED POSE: { x: %f, y: %f, z: %f }", x, y, z);
}

void NeedPoseNavGoal(const actionlib_msgs::GoalStatusArrayConstPtr& msg)
{
  ROS_INFO("GOT CALLBACK");
  static bool first_run = true;
  if (first_run && msg->status_list.size() == 0)
  {
    PublishStandardPose();
    first_run = false;
    return;
  }
  for (size_t i = 0; i < msg->status_list.size(); i++)
  {
    actionlib_msgs::GoalStatus obj = msg->status_list[i];
    if (obj.status == obj.RECALLED || obj.status == obj.REJECTED)
    {
      ROS_INFO("CATCHED ERROR: %s", obj.goal_id.id.c_str());
      MoveBehind();
    }
    if (obj.status == obj.ABORTED)
    {
      ROS_INFO("CATCHED ERROR: %s", obj.goal_id.id.c_str());
      MoveBehind();
      PublishStandardPose();
    }
  }
}
